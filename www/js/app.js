// OPERATIONS ON DOM

// CREATING CUSTOM TAG ELEMENT

// How to create a Web Component using Vanilla JS
// https://www.youtube.com/watch?v=vLkPBj9ZaU0


class Tomcard extends HTMLElement {
  constructor(){
    super();
    this.innerText = this.getAttribute('name');   
  }
}

window.customElements.define('tom-tag', Tomcard);



// SWTICH MODE 

function myFunction() {
  var element = document.body;
  element.classList.toggle("dark-mode");
}

// DISPLAY TIME
let timer = document.querySelector("#time span");

setInterval(()=>{
  timer.innerText = new Date().toLocaleString(),1000
});

// ADDING ELEMENTS TO THE PAGE

// create random number

function random(){
divElement = document.createElement("ol"); 
divElement.innerHTML = Math.random();
document.body.appendChild(divElement);
k1 = divElement.innerHTML ;
k2 = divElement ;
console.log(typeof(k1));
console.log(typeof(k2));
var buttons = document.getElementsByTagName("button");
buttons[1].after(divElement);
console.log(buttons);
if(Math.random() > 0.5){
  throw new Error('This time you received this error')
}
};

// create list with random number and string 

const list = document.querySelector('.list');

function typed(){
  let newText = document.createElement('LI');
  newText.innerHTML = `${Math.random()}, ${"Creating list of random numbers..."}`
  var buttons = document.getElementsByTagName("button");
  buttons[2].after(newText);

}

// create p with string

  function create(){
  let tex = document.body;
  let text = document.createElement("p");
  // inner HTML allows to render/modify the string for inside your HTML elements. Example <b>
  text.innerHTML = "<b>text created by append method with innerHTML </b>. inner HTML allows to render/modify the string for inside your HTML elements";
  tex.append(text);
  console.log(text.innerHTML)
  var buttons = document.getElementsByTagName("button");
  buttons[3].after(text);
  }

// create ol with string

function create2(){
  let tex2 = document.body;
  let text2 = document.createElement("ol");
  text2.textContent = "text created by append method with textContent";
  tex2.append(text2);
  console.log(text2.textContent);
  var buttons = document.getElementsByTagName("button");
  buttons[4].after(text2);
}

// create p with string
// you can create string just one time? why?
function create3(){
  let text3 = document.body
   let text3Div = document.querySelector("#create3");
   text3Div.innerText = "created by appendchild method with innerText"
   text3.appendChild(text3Div);
   let buttons = document.getElementsByTagName("button");
   buttons[5].after(text3Div);
}

//  Delete elements

function rem1(){
  if (span1 = document.querySelector('#span1')) {
  span1.remove();
    console.log(`above element is removed:`, span1);
  } else {
    error = "stop it!"
    alert(`${error} You can't delete it two times`);
    console.error("You can't delete it two times");
  }
}

// Get atribute

function modify1(){
  let modify1 = document.querySelector('#modify1');
  console.log(modify1.getAttribute("title"));
  console.log(modify1.title);
}

// Set/modify atribute

function modify2(){
  let modify2 = document.querySelector('#modify2');

  modify2.setAttribute("title", "title atribute of span 2 now is modyfied");

  console.log(modify2.getAttribute("title"));
    // you can change/modify an atribute like this below
    console.log(modify2.title = "diffrent atribute")
}

// Remove atributes atribute

function modify3(){
  let modify3 = document.querySelector('#modify3');

  modify3.removeAttribute("title");

  console.log(modify3.getAttribute("title"), `now title atribute is removed`);
    // you can change/modify an atribute like this below
 
}

//  Adding a class name

function modify4(){
  const modify4 = document.querySelector('.modify4');

  modify4.classList.add("new-class");
  console.log(modify4.getAttribute("class"))
  
}

// Removing a class


function modify5(){
  const modify5 = document.querySelector('.modify5');

  modify5.classList.remove("modify5");
  console.log(modify5.getAttribute("class"));
  
}

// Toggle a class name
// it removes or adds class 

function modify6(){
  const modify6 = document.querySelector('.modify6');

  modify6.classList.toggle("modify-a-class", true);
  console.log(modify6.getAttribute("class"));
  
}
// Modify style

function modify7(){
  const modify7 = document.querySelector('.modify7');

  modify7.style.color = "blue";

  console.log("font color changed to blue")
};


// Window close

function functionOne(){
  var newWindow = window.open();
  newWindow.close();
    console.log("close");
  }


